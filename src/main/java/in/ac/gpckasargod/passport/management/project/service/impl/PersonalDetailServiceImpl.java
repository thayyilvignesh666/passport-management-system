/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.passport.management.project.service.impl;

import in.ac.gpckasargod.passport.management.project.Service.PersonalDetailService;
import in.ac.gpckasargod.passport.management.project.ui.model.data.PersonalDetail;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class PersonalDetailServiceImpl extends ConnectionService implements PersonalDetailService{
    public String savePersonalDetailService(Integer Application_id,String Name,String Address,String Place,Date Dob,Integer Age,String Gender,Integer MobileNo,String Email,String Nationality) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO PERSONAL_DETAILS(APLLICATION_ID,NAME,ADDRESS,PLACE,DOB,AGE,GENDER,MOBILENO,EMAIL,NATIONALITY) VALUES ('"+applicationid+'",'"+name+'",'"+address+'",'"+place+'",'"+dob+'",'"+age+'",'"+gender+'",'"+mobileno+'",'"+email+'",'"+nationality+'")";
            System.err.println("Query+":query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "Saved failed";
            }else{
                return "Saved succesfully"
            }
            
            
            
           
        } catch (SQLException ex) {
            Logger.getLogger(PersonalDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Save failed"
                }
   @Override
     public PersonalDetail readPersonalDetailService(Integer Id){
         personaldetail PersonalDetail = null;
         try
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query="SELECT FROM *FROM PERSONAL WHERE ID=+id";
             resultSet resultSet = statement.executeQuery(query);
             while(resultSet.next()){
             int personalApplicationId = resultSet.getint("APPLICAION ID")
             String personalName = resultSet.getString("NAME");
             String personalAddress=resultSet.getString("ADDRESS");
             String personalPlace = resultSet.getString("PLACE");
             Date personalDob = resultSet.getDate("DOB");
             int personalAge = resultSet.getint("AGE");
             String personalGender = resultSet.getstring("GENDER");
             int personalMobileNo = resultSet.getint("MobileNo");
             String personalEmail = resultSet.getstring("EMAIL");
             String personalNationality = resultSet.getstring("NATIONALITY");    
        } catch (SQLException ex) {
            Logger.getLogger(PersonalDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return PersonalDetail;
       
    }
    @Override
    public List<PersonalDetail> getAllPersonalDetailService(){
        List<CPersonalDetail> PersonalDetail = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM ";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String network_Type = resultSet.getString("NETWORK_TYPE");
                String reason = resultSet.getString("REASON");
                String description = resultSet.getString("COMPLAINT");
                Complaint complaint = new Complaint(id,network_Type,reason,description);
                complaints.add(P);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(CPersonal Details ServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return PersonalDetail;
    
    }
    
    @Override
    public String updateComplaint(Integer id,String networktype,String reason,String complaint){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE COMPLAINT SET NETWORK_TYPE='"+networktype+"',REASON='"+reason+"',COMPLAINT='"+complaint+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }

    @Override
    public String deleteComplaint(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM COMPLAINT WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }
    
                
}
