/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.passport.management.project.ui.model.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class PassportDetail {
    private Integer PersonId;
    private Integer PassportId;
    private Date DateOfIssue;

    public PassportDetail(Integer PersonId, Integer PassportId, Date DateOfIssue, Date DateOfExpiry) {
        this.PersonId = PersonId;
        this.PassportId = PassportId;
        this.DateOfIssue = DateOfIssue;
        this.DateOfExpiry = DateOfExpiry;
    }
    private Date DateOfExpiry;

    public Integer getPersonId() {
        return PersonId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getPassportId() {
        return PassportId;
    }

    public void setPassportId(Integer PassportId) {
        this.PassportId = PassportId;
    }

    public Date getDateOfIssue() {
        return DateOfIssue;
    }

    public void setDateOfIssue(Date DateOfIssue) {
        this.DateOfIssue = DateOfIssue;
    }

    public Date getDateOfExpiry() {
        return DateOfExpiry;
    }

    public void setDateOfExpiry(Date DateOfExpiry) {
        this.DateOfExpiry = DateOfExpiry;
    }
    
}
