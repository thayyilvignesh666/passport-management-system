/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.passport.management.project.ui.model.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class PersonalDetail {
    private Integer ApplicantId;
    private String Name;
    private String Address;
    private String Place;
    private Date Dob;
    private Integer Age;
    private  String Gender;
    private  Integer MobileNo;
    private  String Email;
    private String Nationality;

    public PersonalDetail(Integer ApplicantId, String Name, String Address, String Place, Date Dob, Integer Age, String Gender, Integer MobileNo, String Email, String Nationality) {
        this.ApplicantId = ApplicantId;
        this.Name = Name;
        this.Address = Address;
        this.Place = Place;
        this.Dob = Dob;
        this.Age = Age;
        this.Gender = Gender;
        this.MobileNo = MobileNo;
        this.Email = Email;
        this.Nationality = Nationality;
    }

    public Integer getApplicant_Id() {
        return ApplicantId;
    }

    public void setApplicant_Id(Integer Applicant_Id) {
        this.ApplicantId = Applicant_Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String Place) {
        this.Place = Place;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date Dob) {
        this.Dob = Dob;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public Integer getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(Integer MobileNo) {
        this.MobileNo = MobileNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String Nationality) {
        this.Nationality = Nationality;
    }

    
    }

    