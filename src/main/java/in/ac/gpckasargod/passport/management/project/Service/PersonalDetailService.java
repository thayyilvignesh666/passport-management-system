/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.passport.management.project.Service;

import in.ac.gpckasargod.passport.management.project.ui.PersonalDetailsForm;
import java.awt.List;
import java.sql.Date;

/**
 *
 * @author student
 */
public interface PersonalDetailService {
       public String savePersonalDetail(Integer Applicationid,String Name,String Address,String place,Date Dob,Integer Age,String Gender,Integer MobileNo,String Email,String Nationality);
       public PersonalDetail readPersonalDetail(Integer id);
       public List<PersonalDetail> getAllPersonalDetail();
       public String updatePersonalDetail(PersonalDetail personalDetail);
       public String updatePersonalDetail(Integer id,String Name,String Addess,String place,Date Dob,Integer Age,String Gender,Integer MobileNo,String Email,String Nationality);
       public String deletePersonalDetail(Integer id);
    
}
